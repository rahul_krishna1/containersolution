
FROM alpine:3.6

RUN apk add --no-cache nginx-mod-http-lua


# Delete default config
RUN rm -r /etc/nginx/conf.d && rm /etc/nginx/nginx.conf

# Create folder for PID file
RUN mkdir -p /run/nginx
COPY ./nginx.conf /etc/nginx/nginx.conf
#COPY ./nginx.conf.template /etc/nginx/nginx.conf.template
RUN  ln -sf /proc/self/fd /dev/

CMD ["nginx"]
